///Theory

// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.
// Чтобы использовать специальный символ как обычный, нужно добавить к нему обратную косую черту: \. .
// Это называется «экранирование символа». Необходимо для корректного отображения.

// 2. Які засоби оголошення функцій ви знаєте?
// Через ключевое слово function. Самый удобный способ.
// Через переменную, функцию-выражение.
// Через стрелочную функцию.
// Другие более сложные способы.

// 3. Що таке hoisting, як він працює для змінних та функцій?
// С английского поднятие. Функцию можно запустить до того как она была объявлена.
// По переменным работает только с var так, как только он поднимается на верх, но не его значение.

/// Practice
// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
// Технічні вимоги:
//
// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// Створити метод getAge() який повертатиме скільки користувачеві років.
// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.

let firstNamePrompt;
let lastNamePrompt;
let newUser;
let newUserGetLogin;
let value1;
let value2;
let value3;
let yourBirthdayPrompt;


function createNewUser() {
    firstNamePrompt = prompt("Please type your firstName", "");
    lastNamePrompt = prompt("Please type your lastName", "");
    yourBirthdayPrompt = prompt("Please type your birthday", "");
    newUser = {
        getLogin: function () {
            return (firstNamePrompt[0].toLowerCase() + lastNamePrompt.toLowerCase());
        },
        getAge: function() {
            let date = this.birthday;
            let dateArray = date.split("."),
                dd = parseInt(dateArray [0]),
                mm = parseInt(dateArray [1]) - 1,
                yyyy = dateArray [2];
            let currentDate = new Date().getFullYear(),
                userDate = new Date(yyyy, mm, dd).getFullYear();
            if (mm < 1) {
                return (currentDate - userDate);
            } else {
                return (currentDate - userDate - 1);
            }
        },
        getPassword: function() {
            return (firstNamePrompt[0].toUpperCase() + lastNamePrompt.toLowerCase() + yourBirthdayPrompt.slice(6, 10));
        }
    }
    value1 = firstNamePrompt;
    Object.defineProperty(newUser, "firstName", {
        get: () => value1,
        set: (newValue) => value1 = newValue
    });
    value2 = lastNamePrompt;
    Object.defineProperty(newUser, "lastName", {
        get: () => value2,
        set: (newValue) => value2 = newValue
    });
    value3 = yourBirthdayPrompt;
    Object.defineProperty(newUser, "birthday", {
        get: () => value3,
        set: (newValue) => value3 = newValue
    });

    return newUser;
}

newUserGetLogin = createNewUser();

console.log(newUserGetLogin.getLogin());

console.log(newUserGetLogin.getAge());

console.log(newUserGetLogin.getPassword());

